const int WiFiRetryMax=100;

bool MitInternetVerbinden()
{
  Serial.println("Mit Internet verbinden");
  WiFi.mode(WIFI_STA); 
  WiFi.begin(ssid, password);
  int WiFiRetry=0;
  while ((WiFi.status()!=WL_CONNECTED)&&(WiFiRetry<WiFiRetryMax)) 
  {
    delay(100);
    WiFiRetry++;
    Serial.print(WiFiRetry%10);
  }
  if(WiFiRetry>=WiFiRetryMax) 
  {
    Serial.println();
    Serial.print("Verbindung mit ");
    Serial.print(ssid);
    Serial.println(" konnte nicht hergestellt werden! :-( ");
    delay(100);
    return false;
  }
  Serial.print(" \r\nVerbunden mit: ");
  Serial.print(ssid);
  Serial.print(" ---> IP ");
  Serial.println(WiFi.localIP());
  delay(100);
  return true;
}   

void LokalNetStarten()
{
  Serial.println("Mit Lokal-Net verbinden");
  WiFi.mode(WIFI_STA); // WIFI_AP, WIFI_STA, WIFI_AP_STA or WIFI_OFF.
  WiFi.config(ip, gateway, subnet);
  WiFi.begin(ssid, password);
  int WiFiRetry=0;
  while ((WiFi.status()!=WL_CONNECTED)&&(WiFiRetry<WiFiRetryMax)) 
  {
    delay(100);
    WiFiRetry++;
    Serial.print(WiFiRetry%10);
  }
  if(WiFiRetry==WiFiRetryMax) 
  {
    Serial.println(" \r\n_S_Y_S_T_E_M_-_F_E_H_L_E_R_!_");
    Serial.println("=============================");
    Serial.println();
    Serial.print("Verbindung mit ");
    Serial.print(ssid);
    Serial.println(" konnte nicht hergestellt werden! :-( ");
    Serial.println("Neustart erfolgt!");
    Serial.println();
    delay(5000);
    ESP.restart();
  }
  delay(500);
  Serial.print(" \r\nVerbunden mit: ");
  Serial.print(ssid);
  Serial.print(" ---> IP ");
  Serial.println(WiFi.localIP());
}

void WiFi_Disconnect()   
{
  WiFi.disconnect();
  WiFi.setAutoConnect(false);
  WiFi.mode(WIFI_OFF); // WIFI_AP, WIFI_STA, WIFI_AP_STA or WIFI_OFF.
  Serial.println("WiFi-Verbingung beendet");
}
