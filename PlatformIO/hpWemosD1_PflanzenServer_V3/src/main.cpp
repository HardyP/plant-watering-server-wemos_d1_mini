#include <Arduino.h>
#include <TimeLib.h> // https://github.com/PaulStoffregen/Time
#include <time.h>
#include <ESPAsyncTCP.h>
#include <WiFiUdp.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <SPI.h> 
#include "SdFat.h"
#include "sdios.h"
#include <../../ssid_password.h> // Pfad, ssid und password anpassen
#include <setup_daten.h> // Werte ggf anpassen


using namespace sdfat;

//#define DEBUG
#ifdef DEBUG
  #define DPRINT(x)  Serial.print(x)
  #define DPRINTLN(x)  Serial.println(x)
#else
  #define DPRINT(x)
  #define DPRINTLN(x)
#endif

/*
SD Shield benutzt SPI-Bus:
 * D5 = CLK
 * D6 = MISO
 * D7 = MOSI
 * D8 = CS
*/

const uint8_t chipSelect = D8;

ESP8266WebServer server(80);
WiFiUDP Udp;

int LED=D4;  // LED_BUILTIN
const int PacketSizeMax=120;
char incomingPacket[PacketSizeMax]; // Puffer für eingehende Pakete
char replyPacket[PacketSizeMax]; // Puffer für ausgehende Pakete
time_t Zeit;
time_t TimeLastUDP=0;
time_t DiffTimeLastUDP=0;
time_t TimeToRestart=0;
int AnzTimeLastUDP=0;
String SensorKennung;
String sServerZeit;
String Alias;
float Temperatur;
float TemperaturD12StundenCount;
float TemperaturD1TageCount;
float TemperaturD2TageCount;
float TemperaturD3TageCount;
float TemperaturD7TageCount;
float TemperaturD14TageCount;
float TemperaturD30TageCount;
float Spannung;
uint16_t Feuchte;
uint16_t MinFeuchte;
uint16_t Wasser;
uint16_t SchlafZeit;
uint16_t WartenBisWasser;
uint16_t WartenBisWasserS;
uint16_t PumpZeit;
String tmp;
char LineBuffer[256];
char buffer[256];
char CopyBuf[512]; 
int CopyBufLen;
String LineString;
String SaveLineString;

SdFat sd;
SdFile ChirpFile;
SdFile ChirpBackupFile;
SdFile ChirpSollWerteFile;
SdFile VorlageFile;
SdFile WebSiteFile;
SdFile WebSiteClientSetupFile;
SdFile StreamFile; 
SdFile SourceFile;
SdFile DestFile;

const char* ChirpFileName="/chirpdaten.txt";
const char* VorlageFileName="/vorlage.html";
const char* WebSiteFileName="/index.html";
const long MaxClients=SensorenMax;
const time_t Sekunden12h=12*60*60;
const time_t Sekunden1d=Sekunden12h*2;
const time_t Sekunden2d=Sekunden1d*2;
const time_t Sekunden3d=Sekunden1d*3;
const time_t Sekunden7d=Sekunden1d*7;
const time_t Sekunden14d=Sekunden1d*14;
const time_t Sekunden30d=Sekunden1d*30;
uint32_t FileSize;
char WebSiteClientSetupFileName[20]; 
char ChirpBackupFileName[20];
String StreamPath;
String StreamDataType;
char StreamBuf[1024]; 
int StreamBufLen;
long SetClientNr=MaxClients+1;
int SelSens=0; // selektierter Sensor
int SensorenAnzahl=0; // Anzahl erkannter Sensoren
char HeadStr[3500];
char TabStr[30000];

typedef struct 
{
char SensorKennung[20];
char Alias[31]="";
time_t Zeit;
char tZeit[18];
time_t WasserZeit;
char tWasserZeit[10];
uint16_t Feuchte=0;
uint16_t MinFeuchte=0;
float Temperatur=0;
float Spannung=0;
uint16_t SchlafZeit=0;
uint16_t WartenBisWasser=0;
uint16_t WartenBisWasserS=0;
uint16_t PumpZeit=0;
uint16_t Wasser=0;
uint16_t Wasser12Stunden=0;
uint16_t Wasser1Tage=0;
uint16_t Wasser2Tage=0;
uint16_t Wasser3Tage=0;
uint16_t Wasser7Tage=0;
uint16_t Wasser14Tage=0;
uint16_t Wasser30Tage=0;
float TemperaturMax12Stunden=0;
float TemperaturMax1Tage=0;
float TemperaturMax2Tage=0;
float TemperaturMax3Tage=0;
float TemperaturMax7Tage=0;
float TemperaturMax14Tage=0;
float TemperaturMax30Tage=0;
float TemperaturMin12Stunden=0;
float TemperaturMin1Tage=0;
float TemperaturMin2Tage=0;
float TemperaturMin3Tage=0;
float TemperaturMin7Tage=0;
float TemperaturMin14Tage=0;
float TemperaturMin30Tage=0;
float TemperaturD12Stunden=0;
float TemperaturD1Tage=0;
float TemperaturD2Tage=0;
float TemperaturD3Tage=0;
float TemperaturD7Tage=0;
float TemperaturD14Tage=0;
float TemperaturD30Tage=0;
} SensorRecord;
SensorRecord SensorenDaten[SensorenMax];

#include <WifiUtility.h>
#include <WebSiteErzeugen.h>
#include <ServerZeitHolen.h>
#include <FileSystemStarten.h>
#include <ChirpDatenAnalyse.h>

void handleGenericArgs()
{
  String message = "Anzahl empfangene Argumente: ";
  message += server.args();
  message += "\n";
  for (int i=0; i<server.args(); i++)
  {
    message+="ArgumentNr "+(String)i+" –> ";
    message+=server.argName(i)+": ";
    message+=server.arg(i)+"\n";
  } 
  Serial.print(message);
}

void handleIndex()
{
  if (SetClientNr<MaxClients+1) 
  {
    handleGenericArgs();
    if (server.arg("action")=="1")
    {
      Serial.println("OK gedrückt");
      server.arg("alias").toCharArray(SensorenDaten[SetClientNr].Alias,31);
      SensorenDaten[SetClientNr].SchlafZeit=server.arg("c_pause").toInt()*60; 
      SensorenDaten[SetClientNr].WartenBisWasserS=server.arg("c_wasser").toInt()*60; 
      SensorenDaten[SetClientNr].PumpZeit=server.arg("c_pump").toInt(); 
      SensorenDaten[SetClientNr].MinFeuchte=server.arg("c_feuchte").toInt(); 
    }
    else {Serial.println("Abbruch gedrückt");}
    WebSiteErzeugen();
    Serial.println(" ---------------------------------");
  }
  SetClientNr=MaxClients+1;
  StreamPath=WebSiteFileName;
  StreamDataType="text/html";
  Serial.print("handleIndex-StreamPath --> ");
  Serial.println(StreamPath);
  if (StreamFile.open(StreamPath.c_str()))
  {  
    while (StreamFile.available()) 
    {
      StreamBufLen=StreamFile.read(StreamBuf,sizeof(StreamBuf));
      server.client().write((const char*)StreamBuf,StreamBufLen);
    }
    StreamFile.close();
  }
  else
  {
    String message = "F E H L E R ! Konnte 'index.html' nicht oeffnen!\n\n";
    server.send(404, "text/plain", message);
    Serial.print(message);
  }
}

void handleClientSetup()
{
  Serial.println(" -----> handleClientSetup <-----");
  handleGenericArgs();
  SetClientNr=server.arg(0).toInt();
  Serial.print("Bearbeitung ClientNr.: ");
  Serial.println(SetClientNr);
  WebSiteConfigErzeugen();
  StreamPath=WebSiteClientSetupFileName;
  StreamDataType="text/html";
  Serial.print("handleClientSetup-StreamPath --> ");
  Serial.println(StreamPath);
  if (StreamFile.open(StreamPath.c_str()))
  {  
    while (StreamFile.available()) 
    {
      StreamBufLen=StreamFile.read(StreamBuf,sizeof(StreamBuf));
      server.client().write((const char*)StreamBuf,StreamBufLen);
    }
    StreamFile.close();
  }
  else
  {
    String message = "F E H L E R ! Konnte 'ClientSetupXX' nicht oeffnen!\n\n";
    server.send(404, "text/plain", message);
    Serial.print(message);
  }
}

void handleNotFound()
{
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += server.uri();
  message += "\nMethod: ";
  message += (server.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += server.args();
  message += "\n";
  for (uint8_t i=0; i<server.args(); i++)
    {message +=" "+ server.argName(i)+": "+server.arg(i)+"\n";}
  server.send(404, "text/plain", message);
  Serial.print(message);
}

void SelSensErmitteln()
{
  bool Gefunden=false;
  char Kennung[20];
  SensorKennung.toCharArray(Kennung,20);
  if (SensorenAnzahl>0)
  {
    SelSens=-1;
    while ((!Gefunden)&&(SensorenAnzahl!=SelSens+1)) 
    {
      SelSens++;
      if (strcmp(Kennung,SensorenDaten[SelSens].SensorKennung)==0) {Gefunden=true;}
    }
    if (!Gefunden) 
    {
      SensorenAnzahl++;
      SelSens=SensorenAnzahl-1;
    }
  }
  else
  {
    SensorenAnzahl=1;
    SelSens=0;
  }
  if (!Gefunden) 
  {
    SensorKennung.toCharArray(SensorenDaten[SelSens].SensorKennung,20);
    Alias.toCharArray(SensorenDaten[SelSens].Alias,31);
    SensorenDaten[SelSens].MinFeuchte=MinFeuchte;
    SensorenDaten[SelSens].SchlafZeit=SchlafZeit;
    SensorenDaten[SelSens].WartenBisWasserS=WartenBisWasserS;
    SensorenDaten[SelSens].PumpZeit=PumpZeit; 
  }
  SensorenDaten[SelSens].Zeit=Zeit;
  strftime(SensorenDaten[SelSens].tZeit,18,"%d.%m.%Y-%H:%M",localtime(&Zeit));
  SensorenDaten[SelSens].Feuchte=Feuchte;
  SensorenDaten[SelSens].Wasser=Wasser;
  SensorenDaten[SelSens].Spannung=Spannung;
  SensorenDaten[SelSens].Temperatur=Temperatur;
  SensorenDaten[SelSens].WartenBisWasser=WartenBisWasser;
}

void UdpEmpfangBestaetigen()
{
  yield();
  strcpy(replyPacket,":-)");
  strcat(replyPacket,";");
  itoa(SensorenDaten[SelSens].MinFeuchte,buffer,10);
  strcat(replyPacket,buffer);
  strcat(replyPacket,";");
  itoa(SensorenDaten[SelSens].SchlafZeit,buffer,10);
  strcat(replyPacket,buffer);
  strcat(replyPacket,";");
  itoa(SensorenDaten[SelSens].WartenBisWasserS,buffer,10);
  strcat(replyPacket,buffer);
  strcat(replyPacket,";");
  itoa(SensorenDaten[SelSens].PumpZeit,buffer,10);
  strcat(replyPacket,buffer);
  strcat(replyPacket,";");
  strcat(replyPacket,SensorenDaten[SelSens].Alias);
  strcat(replyPacket,";");
  if (AnzTimeLastUDP>0) 
  {
    AnzTimeLastUDP--;
    Serial.print("AnzTimeLastUDP: "); Serial.println(AnzTimeLastUDP);
  } 
  if ((DiffTimeLastUDP<=MinDiffTimeUDP)&&(AnzTimeLastUDP<1))
  {
    AnzTimeLastUDP=SensorenAnzahl;
    itoa(AddDiffTimeUDP,buffer,10);
    Serial.print(AddDiffTimeUDP); Serial.println(" Sekunden Extra-Schlafzeit!");
  }
  else {itoa(0,buffer,10);} 
  strcat(replyPacket,buffer);
  strcat(replyPacket,";");
  Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
  Udp.write(replyPacket);
  Udp.endPacket();
  Serial.printf("UDP-Paketinhalt: %s\n",replyPacket);
  Serial.printf("OK-Empfangsbestätigung an %s versendet!\n",Udp.remoteIP().toString().c_str());
}

void UdpPacket() 
{
  int packetSize=Udp.parsePacket();
//  yield();
  if (packetSize)
  {
    digitalWrite(LED,LOW);                     
    Serial.printf("UDP-Paket mit %d bytes von %s, port %d empfangen!\n",packetSize,Udp.remoteIP().toString().c_str(),Udp.remotePort());
    int len=Udp.read(incomingPacket,PacketSizeMax);
    if (len>0) {incomingPacket[len]=0;}
    Serial.printf("UDP-Paketinhalt: %s\n",incomingPacket);
    DiffTimeLastUDP=now()-TimeLastUDP;
    TimeLastUDP=now();
    Serial.print("DiffTimeLastUDP: "); Serial.println(DiffTimeLastUDP);
    sServerZeit=String(now());
    LineString=sServerZeit+';'+incomingPacket;
    ChirpZeileAnalysieren();
    if (ChirpFile.open(ChirpFileName,FILE_WRITE))
    {
      ChirpFile.println(SaveLineString); 
      Serial.print("Gespeichert: ");
      Serial.println(SaveLineString);
    }
    else
    {
      Serial.println("Filesystem _F_E_H_L_E_R_");
      Serial.println("========================");
      Serial.print(" \r\nDatei: ");
      Serial.print(ChirpFileName);
      Serial.println(" konnte für Append nicht geöffnet werden!");
      Serial.println("Neustart erfolgt!");
      Serial.println();
      sd.initErrorHalt();
      ChirpFile.close();
      delay(5000);
      ESP.restart();
    }
    ChirpFile.close();
    delay(50);
    SelSensErmitteln();
    delay(50);
    UdpEmpfangBestaetigen();
    delay(50);
    ChirpDatenAnalyse();
    delay(50);
    WebSiteErzeugen();
    digitalWrite(LED,HIGH);
  }
}

float MesseSpannung()
{
  unsigned int raw=0;
  pinMode(A0,INPUT);
  raw=analogRead(A0);
  delay(1000);
  return raw/1023.0*4.2;
}

void ggfRestart()
{
  if (now()>TimeToRestart)
  {
    Serial.println(" \r\n");
    Serial.println("!=====================================!");
    Serial.println("!  PflanzenServer wird neu gestartet  !"); 
    Serial.println("!=====================================!");
    Serial.println();
    Serial.println();
    ESP.restart();
  }
}

void setup() 
{
  pinMode(LED,OUTPUT); 
  // Intro    
  digitalWrite(LED,LOW); delay(200); digitalWrite(LED,HIGH); delay(400);                      
  digitalWrite(LED,LOW); delay(200); digitalWrite(LED,HIGH); delay(400);                      
  digitalWrite(LED,LOW); delay(200); digitalWrite(LED,HIGH); delay(400);                      
  digitalWrite(LED,LOW); delay(200); digitalWrite(LED,HIGH); delay(400);                      
  // Ab geht es ...
  delay(50);
  Serial.begin(115200);
  delay(20000);
   
  Serial.println(" \r\n");
  Serial.println("!===============================!");
  Serial.println("!  hpWeMosD1 PflanzenServer V3  !"); 
  Serial.println("!===============================!");
  RTC_starten();
  ServerZeitHolen();
  FileSystemStarten();
  LokalNetStarten();
  ChirpDatenAnalyse();
  WebSiteHeadErzeugen();
  WebSiteErzeugen();
  
  server.on("/",HTTP_GET,handleIndex);
  server.on("/ClientSetup",HTTP_GET,handleClientSetup);

  server.begin();
  Serial.println("HTTP Server wurde gestartet!");
  if (Udp.begin(localUdpPort)) {Serial.println("UDP wurde gestartet!");} else {Serial.println("UDP konnte nicht gestartet werden!");}
   Serial.printf("Nun lauschen auf IP %s, UDP-Port %d\n", WiFi.localIP().toString().c_str(), localUdpPort);
}

void loop(void) 
{
  server.handleClient();
  delay(50);
  UdpPacket();
  ggfSyncMitRTC();
  ggfRestart();
}
