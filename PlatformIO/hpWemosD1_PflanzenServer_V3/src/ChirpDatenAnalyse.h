void ChirpZeileAnalysieren()
{
  yield();
  int Colon1=LineString.indexOf(';');
  int Colon2=LineString.indexOf(';',Colon1+1);
  int Colon3=LineString.indexOf(';',Colon2+1);
  int Colon4=LineString.indexOf(';',Colon3+1);
  int Colon5=LineString.indexOf(';',Colon4+1);
  int Colon6=LineString.indexOf(';',Colon5+1);
  int Colon7=LineString.indexOf(';',Colon6+1);
  int Colon8=LineString.indexOf(';',Colon7+1);
  int Colon9=LineString.indexOf(';',Colon8+1);
  int Colon10=LineString.indexOf(';',Colon9+1);
  int Colon11=LineString.indexOf(';',Colon10+1);
  int Colon12=LineString.indexOf(';',Colon11+1);
  tmp=LineString.substring(0,Colon1);
  Zeit=tmp.toInt();
  SensorKennung=LineString.substring(Colon1+1,Colon2);
  tmp=LineString.substring(Colon2+1,Colon3);
  Spannung=tmp.toFloat()/100;
  tmp=LineString.substring(Colon3+1,Colon4);
  Temperatur=tmp.toFloat()/10;
  tmp=LineString.substring(Colon4+1,Colon5);
  Wasser=tmp.toInt();
  tmp=LineString.substring(Colon5+1,Colon6);
  Feuchte=tmp.toInt();
  tmp=LineString.substring(Colon6+1,Colon7);
  MinFeuchte=tmp.toInt();
  tmp=LineString.substring(Colon7+1,Colon8);
  SchlafZeit=tmp.toInt();
  tmp=LineString.substring(Colon8+1,Colon9);
  WartenBisWasser=tmp.toInt();
  tmp=LineString.substring(Colon9+1,Colon10);
  WartenBisWasserS=tmp.toInt();
  tmp=LineString.substring(Colon10+1,Colon11);
  PumpZeit=tmp.toInt();
  Alias=LineString.substring(Colon11+1,Colon12);
  SaveLineString=LineString.substring(0,Colon6)+";";
}
 
void SaveChirpZeileAnalysieren()
{
  int Colon1=LineString.indexOf(';');
  int Colon2=LineString.indexOf(';',Colon1+1);
  int Colon3=LineString.indexOf(';',Colon2+1);
  int Colon4=LineString.indexOf(';',Colon3+1);
  int Colon5=LineString.indexOf(';',Colon4+1);
  int Colon6=LineString.indexOf(';',Colon5+1);
  tmp=LineString.substring(0,Colon1);
  Zeit=tmp.toInt();
  SensorKennung=LineString.substring(Colon1+1,Colon2);
  tmp=LineString.substring(Colon2+1,Colon3);
  Spannung=tmp.toFloat()/100;
  tmp=LineString.substring(Colon3+1,Colon4);
  Temperatur=tmp.toFloat()/10;
  tmp=LineString.substring(Colon4+1,Colon5);
  Wasser=tmp.toInt();
  tmp=LineString.substring(Colon5+1,Colon6);
  Feuchte=tmp.toInt();
}

void ChirpDatenAnalyse()
{
  char Kennung[20];
  time_t Stunden12;
  time_t Tage1;
  time_t Tage2;
  time_t Tage3;
  time_t Tage7;
  time_t Tage14;
  time_t Tage30;
  time_t WasserZeit=0;
  int WasserZeitM=0;
  int WasserZeitH=0;
  int WasserZeitD=0;
  Stunden12=now()-Sekunden12h;
  Tage1=now()-Sekunden1d;
  Tage2=now()-Sekunden2d;
  Tage3=now()-Sekunden3d;
  Tage7=now()-Sekunden7d;
  Tage14=now()-Sekunden14d;
  Tage30=now()-Sekunden30d;
  SensorenDaten[SelSens].Wasser12Stunden=0;
  SensorenDaten[SelSens].Wasser1Tage=0;
  SensorenDaten[SelSens].Wasser2Tage=0;
  SensorenDaten[SelSens].Wasser3Tage=0;
  SensorenDaten[SelSens].Wasser7Tage=0;
  SensorenDaten[SelSens].Wasser14Tage=0;
  SensorenDaten[SelSens].Wasser30Tage=0;
  SensorenDaten[SelSens].WasserZeit=0;
  SensorenDaten[SelSens].TemperaturMax12Stunden=0;
  SensorenDaten[SelSens].TemperaturMax1Tage=0;
  SensorenDaten[SelSens].TemperaturMax2Tage=0;
  SensorenDaten[SelSens].TemperaturMax3Tage=0;
  SensorenDaten[SelSens].TemperaturMax7Tage=0;
  SensorenDaten[SelSens].TemperaturMax14Tage=0;
  SensorenDaten[SelSens].TemperaturMax30Tage=0;
  SensorenDaten[SelSens].TemperaturMin12Stunden=100;
  SensorenDaten[SelSens].TemperaturMin1Tage=100;
  SensorenDaten[SelSens].TemperaturMin2Tage=100;
  SensorenDaten[SelSens].TemperaturMin3Tage=100;
  SensorenDaten[SelSens].TemperaturMin7Tage=100;
  SensorenDaten[SelSens].TemperaturMin14Tage=100;
  SensorenDaten[SelSens].TemperaturMin30Tage=100;
  SensorenDaten[SelSens].TemperaturD12Stunden=0;
  SensorenDaten[SelSens].TemperaturD1Tage=0;
  SensorenDaten[SelSens].TemperaturD2Tage=0;
  SensorenDaten[SelSens].TemperaturD3Tage=0;
  SensorenDaten[SelSens].TemperaturD7Tage=0;
  SensorenDaten[SelSens].TemperaturD14Tage=0;
  SensorenDaten[SelSens].TemperaturD30Tage=0;
  TemperaturD12StundenCount=0;
  TemperaturD1TageCount=0;
  TemperaturD2TageCount=0;
  TemperaturD3TageCount=0;
  TemperaturD7TageCount=0;
  TemperaturD14TageCount=0;
  TemperaturD30TageCount=0;
  ChirpFile.open(ChirpFileName,FILE_READ);
  while (ChirpFile.available()) 
  {
    yield();
    ChirpFile.fgets(LineBuffer,sizeof(LineBuffer));
    LineString=LineBuffer;
    SaveChirpZeileAnalysieren();
    SensorKennung.toCharArray(Kennung,20);
    if (strcmp(Kennung,SensorenDaten[SelSens].SensorKennung)==0)       
    {
      if (Wasser>0) {WasserZeit=now()-Zeit;}
      if (Zeit>=Stunden12) 
      {
        SensorenDaten[SelSens].Wasser12Stunden+=Wasser;
        if (Temperatur>SensorenDaten[SelSens].TemperaturMax12Stunden) {SensorenDaten[SelSens].TemperaturMax12Stunden=Temperatur;}
        if (Temperatur<SensorenDaten[SelSens].TemperaturMin12Stunden) {SensorenDaten[SelSens].TemperaturMin12Stunden=Temperatur;}
        TemperaturD12StundenCount++;
        SensorenDaten[SelSens].TemperaturD12Stunden+=Temperatur;
      }
      if (Zeit>=Tage1) 
      {
        SensorenDaten[SelSens].Wasser1Tage+=Wasser;
        if (Temperatur>SensorenDaten[SelSens].TemperaturMax1Tage) {SensorenDaten[SelSens].TemperaturMax1Tage=Temperatur;}
        if (Temperatur<SensorenDaten[SelSens].TemperaturMin1Tage) {SensorenDaten[SelSens].TemperaturMin1Tage=Temperatur;}
        TemperaturD1TageCount++;
        SensorenDaten[SelSens].TemperaturD1Tage+=Temperatur;
      }
      if (Zeit>=Tage2) 
      {
        SensorenDaten[SelSens].Wasser2Tage+=Wasser;
        if (Temperatur>SensorenDaten[SelSens].TemperaturMax2Tage) {SensorenDaten[SelSens].TemperaturMax2Tage=Temperatur;}
        if (Temperatur<SensorenDaten[SelSens].TemperaturMin2Tage) {SensorenDaten[SelSens].TemperaturMin2Tage=Temperatur;}
        TemperaturD2TageCount++;
        SensorenDaten[SelSens].TemperaturD2Tage+=Temperatur;
      }
      if (Zeit>=Tage3) 
      {
        SensorenDaten[SelSens].Wasser3Tage+=Wasser;
        if (Temperatur>SensorenDaten[SelSens].TemperaturMax3Tage) {SensorenDaten[SelSens].TemperaturMax3Tage=Temperatur;}
        if (Temperatur<SensorenDaten[SelSens].TemperaturMin3Tage) {SensorenDaten[SelSens].TemperaturMin3Tage=Temperatur;}
        TemperaturD3TageCount++;
        SensorenDaten[SelSens].TemperaturD3Tage+=Temperatur;
      }
      if (Zeit>=Tage7) 
      {
        SensorenDaten[SelSens].Wasser7Tage+=Wasser;
        if (Temperatur>SensorenDaten[SelSens].TemperaturMax7Tage) {SensorenDaten[SelSens].TemperaturMax7Tage=Temperatur;}
        if (Temperatur<SensorenDaten[SelSens].TemperaturMin7Tage) {SensorenDaten[SelSens].TemperaturMin7Tage=Temperatur;}
        TemperaturD7TageCount++;
        SensorenDaten[SelSens].TemperaturD7Tage+=Temperatur;
      }
      if (Zeit>=Tage14) 
      {
        SensorenDaten[SelSens].Wasser14Tage+=Wasser;
        if (Temperatur>SensorenDaten[SelSens].TemperaturMax14Tage) {SensorenDaten[SelSens].TemperaturMax14Tage=Temperatur;}
        if (Temperatur<SensorenDaten[SelSens].TemperaturMin14Tage) {SensorenDaten[SelSens].TemperaturMin14Tage=Temperatur;}
        TemperaturD14TageCount++;
        SensorenDaten[SelSens].TemperaturD14Tage+=Temperatur;
      }
      if (Zeit>=Tage30) 
      {
        SensorenDaten[SelSens].Wasser30Tage+=Wasser;
        if (Temperatur>SensorenDaten[SelSens].TemperaturMax30Tage) {SensorenDaten[SelSens].TemperaturMax30Tage=Temperatur;}
        if (Temperatur<SensorenDaten[SelSens].TemperaturMin30Tage) {SensorenDaten[SelSens].TemperaturMin30Tage=Temperatur;}
        TemperaturD30TageCount++;
        SensorenDaten[SelSens].TemperaturD30Tage+=Temperatur;
      }
    }
  } 
  ChirpFile.close();
  WasserZeitM=WasserZeit/60;
  WasserZeitH=WasserZeitM/60;
  WasserZeitD=WasserZeitH/24;
  WasserZeitM=WasserZeitM-WasserZeitH*60;
  WasserZeitH=WasserZeitH-WasserZeitD*24;
  SensorenDaten[SelSens].WasserZeit=WasserZeit;
  sprintf(LineBuffer,"%02d",WasserZeitD); 
  strcpy(SensorenDaten[SelSens].tWasserZeit,LineBuffer);
  strcat(SensorenDaten[SelSens].tWasserZeit,":");
  sprintf(LineBuffer,"%02d",WasserZeitH); 
  strcat(SensorenDaten[SelSens].tWasserZeit,LineBuffer);
  strcat(SensorenDaten[SelSens].tWasserZeit,":");
  sprintf(LineBuffer,"%02d",WasserZeitM); 
  strcat(SensorenDaten[SelSens].tWasserZeit,LineBuffer);
  SensorenDaten[SelSens].TemperaturD12Stunden/=TemperaturD12StundenCount;
  SensorenDaten[SelSens].TemperaturD1Tage/=TemperaturD1TageCount;
  SensorenDaten[SelSens].TemperaturD2Tage/=TemperaturD2TageCount;
  SensorenDaten[SelSens].TemperaturD3Tage/=TemperaturD3TageCount;
  SensorenDaten[SelSens].TemperaturD7Tage/=TemperaturD7TageCount;
  SensorenDaten[SelSens].TemperaturD14Tage/=TemperaturD14TageCount;
  SensorenDaten[SelSens].TemperaturD30Tage/=TemperaturD30TageCount;
  Serial.println(); 
  Serial.print("  Client: ");           
  Serial.println(SensorenDaten[SelSens].SensorKennung);
  if (strlen(SensorenDaten[SelSens].Alias)>0)
  {
    Serial.print("  ---> ");           
    Serial.print(SensorenDaten[SelSens].Alias);           
    Serial.println(" <---"); 
  }
  Serial.print("  >> "); 
  strftime(buffer,20,"%d.%m.%Y-%H:%M",localtime(&Zeit));
  Serial.print(buffer); 
  Serial.println("  << "); 

  Serial.println(".----------------------------."); 
  Serial.printf("! %16s","letztes Wasser:");           
  Serial.printf("%10s !",SensorenDaten[SelSens].tWasserZeit);           
  Serial.println(); 
  Serial.printf("! %16s","Feuchte:");           
  Serial.printf("%10d !",SensorenDaten[SelSens].Feuchte);           
  Serial.println(); 
  Serial.printf("! %16s","minFeuchte:");           
  Serial.printf("%10d !",SensorenDaten[SelSens].MinFeuchte);           
  Serial.println(); 
  Serial.printf("! %16s","Temperatur:");           
  Serial.printf("%10.1f !",SensorenDaten[SelSens].Temperatur);           
  Serial.println(); 
  Serial.printf("! %16s","Spannung:");           
  Serial.printf("%10.2f !",SensorenDaten[SelSens].Spannung);           
  Serial.println(); 
  Serial.println(" ---------------------------- "); 
  Serial.println(); 
  Serial.printf("%13s","");           
  Serial.printf(" ! %6s","12h");           
  Serial.printf(" ! %6s","1d");           
  Serial.printf(" ! %6s","2d");           
  Serial.printf(" ! %6s","3d");           
  Serial.printf(" ! %6s","7d");           
  Serial.printf(" ! %6s","14d");           
  Serial.printf(" ! %6s !","30d");           
  Serial.println(); 
  Serial.println(".-------------!--------!--------!--------!--------!--------!--------!--------!"); 
  Serial.printf("!%12s","Wasser");           
  Serial.printf(" ! %6d",SensorenDaten[SelSens].Wasser12Stunden);           
  Serial.printf(" ! %6d",SensorenDaten[SelSens].Wasser1Tage);           
  Serial.printf(" ! %6d",SensorenDaten[SelSens].Wasser2Tage);           
  Serial.printf(" ! %6d",SensorenDaten[SelSens].Wasser3Tage);           
  Serial.printf(" ! %6d",SensorenDaten[SelSens].Wasser7Tage);           
  Serial.printf(" ! %6d",SensorenDaten[SelSens].Wasser14Tage);           
  Serial.printf(" ! %6d !",SensorenDaten[SelSens].Wasser30Tage);           
  Serial.println(); 
  Serial.printf("!%12s","+Temperatur");           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturMax12Stunden);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturMax1Tage);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturMax2Tage);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturMax3Tage);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturMax7Tage);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturMax14Tage);           
  Serial.printf(" ! %6.1f !",SensorenDaten[SelSens].TemperaturMax30Tage);           
  Serial.println(); 
  Serial.printf("!%12s","-Temperatur");           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturMin12Stunden);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturMin1Tage);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturMin2Tage);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturMin3Tage);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturMin7Tage);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturMin14Tage);           
  Serial.printf(" ! %6.1f !",SensorenDaten[SelSens].TemperaturMin30Tage);           
  Serial.println(); 
  Serial.printf("!%12s","dTemperatur");           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturD12Stunden);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturD1Tage);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturD2Tage);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturD3Tage);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturD7Tage);           
  Serial.printf(" ! %6.1f",SensorenDaten[SelSens].TemperaturD14Tage);           
  Serial.printf(" ! %6.1f !",SensorenDaten[SelSens].TemperaturD30Tage);           
  Serial.println(); 
  Serial.println(" -------------!--------!--------!--------!--------!--------!--------!-------- "); 
  Serial.println(); 
}
