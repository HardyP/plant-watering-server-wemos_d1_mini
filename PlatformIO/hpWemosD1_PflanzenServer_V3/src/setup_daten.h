const int SensorenMax=10;         // Maximale Anzahl der Clienen
unsigned int localUdpPort=4097;   // Lokaler Port für UDP-Pakete
IPAddress ip(192,168,1,50);       // Feste IP des Pflanzen-Servers, frei wählbar
IPAddress gateway(192,168,1,1);   // Gatway (IP Router eintragen)
IPAddress subnet(255,255,255,0);  // Subnet Maske eintragen
const time_t MinDiffTimeUDP=90;   // minimaler Zeitabstand der UDP-Pakete
const time_t AddDiffTimeUDP=180;  // zusaetzlicher Zeitabstand der UDP-Pakete 
                                  // bei Unterschreitung von MinDeiffTimeUDP

