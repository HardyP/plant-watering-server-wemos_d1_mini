#include <Wire.h> 
#include <RtcDS1307.h>

// CONNECTIONS DS1307:
// DS1307 SDA --> SDA
// DS1307 SCL --> SCL
// DS1307 VCC --> 5v
// DS1307 GND --> GND
RtcDS1307<TwoWire> Rtc(Wire);

// RTC
const time_t Diff1970To2000=946684800;
time_t TimeSyncMitRTC=Diff1970To2000;
time_t TimeRTC;

//NTP
const char* NTP_SERVER = "pool.ntp.org";
//const char* TZ_INFO    = "CET-1CEST-2,M3.5.0/02:00:00,M10.5.0/03:00:00";  // enter your time zone (https://github.com/nayarsystems/posix_tz_db/blob/master/zones.csv)
const char* TZ_INFO    = "CET-1CEST,M3.5.0,M10.5.0/3";  // enter your time zone (https://github.com/nayarsystems/posix_tz_db/blob/master/zones.csv)
time_t TimeNTP;

void PrintDateTime(time_t DateTime)
{
  strftime(buffer,20,"%d.%m.%Y-%H:%M",localtime(&DateTime));
  Serial.print(buffer); 
}

void RTC_starten()  
{
  Serial.println("RTC starten");
  Rtc.Begin();
  if (!Rtc.IsDateTimeValid()) 
  {
    if (Rtc.LastError()!=0)
    {
      Serial.print("RTC communications error = ");
      Serial.println(Rtc.LastError());
    }
    else {Serial.println("RTC lost confidence in the DateTime!");}
  }
  if (!Rtc.GetIsRunning()) {Rtc.SetIsRunning(true);}
  Rtc.SetSquareWavePin(DS1307SquareWaveOut_Low); 
  Serial.print("Gespeicherte RTC-Zeit: ");
  PrintDateTime(Rtc.GetDateTime()+Diff1970To2000+3600);
  Serial.println();
}

bool getNTPtime(int NTP_RetryMax) 
{
  int NTP_Retry=0;
  tm timeinfo;
  time_t NTP_now=0;
  do
  {
    digitalWrite(LED,LOW); delay(250); digitalWrite(LED,HIGH); yield(); delay(250);                      
    digitalWrite(LED,LOW); delay(250); digitalWrite(LED,HIGH); yield(); delay(250);                      
    digitalWrite(LED,LOW); delay(250); digitalWrite(LED,HIGH); yield(); delay(250);                      
    digitalWrite(LED,LOW); delay(250); digitalWrite(LED,HIGH); yield(); delay(250);                      
    NTP_Retry++;
    time(&NTP_now);
    localtime_r(&NTP_now, &timeinfo);
    Serial.print("Retry: ");
    Serial.print(NTP_Retry);
    Serial.print(" --> NTP_Now: ");
    Serial.println(NTP_now);
  } while ((NTP_now<1581600000)&&(NTP_Retry<=NTP_RetryMax)); 
  if(NTP_Retry>=NTP_RetryMax) 
  {
    Serial.println();
    Serial.println("NTP-Zeit konnte nicht ermittelt werden! :-(");
    TimeNTP=0;
    return false;
  }
  TimeNTP=NTP_now;
  Serial.print("NTP - Systemzeit: ");
  PrintDateTime(TimeNTP);
  Serial.println("");
  return true;
}

void RTC_ist_SystemZeit()
{
  setTime(Rtc.GetDateTime()+Diff1970To2000);
  Serial.println("Systemzeit wurde auf RTC-Zeit gesetz!");
  TimeSyncMitRTC=now()+3600; // naechster Sync in 1 Stunde
  Serial.print("Nächste Synchronisation mit RTC: ");
  PrintDateTime(TimeSyncMitRTC);
  Serial.println();
}

void ServerZeitHolen()
{
  time_t t;
  Serial.println("Systemzeit mit NTP synchonisieren");
  Serial.println("=================================");
  configTime(0,0,NTP_SERVER);
  setenv("TZ",TZ_INFO,1);
  if (MitInternetVerbinden())
  {
    if (getNTPtime(20)) 
    {
      setTime(TimeNTP);
      Serial.println("Systemzeit wurde auf NTP-Zeit gesetzt!");
      Rtc.SetDateTime(now()-Diff1970To2000);
      Serial.println("RTC wurde auf NTP-Zeit gesetzt!");
      TimeSyncMitRTC=now()+3600; // naechster Sync in 1 Stunde
      Serial.print("Nächste Synchronisation mit RTC: ");
      PrintDateTime(TimeSyncMitRTC);
      Serial.println();
    } 
    else {RTC_ist_SystemZeit();}
  }
  else {RTC_ist_SystemZeit();}
  t=now();
  TimeToRestart=t-(hour(t)*3600)-(minute(t)*60)-second(t);
  TimeToRestart+=(27*3600+35*60+30); // Restart erfolgt um 4:35Uhr des folgenden Tages
//  TimeToRestart+=(24*3600+35*60+30); // Restart erfolgt um 1:35Uhr des folgenden Tages
  Serial.print("Nächster Restart des Servers: ");
  PrintDateTime(TimeToRestart);
  Serial.println();
  WiFi_Disconnect();
}

void ggfSyncMitRTC()
{
  if (now()>TimeSyncMitRTC)
  {
    Serial.println("Systemzeit mit RTC synchonisieren");
    Serial.println("=================================");
    Serial.print("Aktuelle Systmzeit: ");
    PrintDateTime(now());
    Serial.println();
    Serial.print("Neue Systemzeit (RTC): ");
    PrintDateTime(Rtc.GetDateTime()+Diff1970To2000);
    Serial.println();
    setTime(Rtc.GetDateTime()+Diff1970To2000);
    TimeSyncMitRTC=now()+3600; // naechster Sync in 1 Stunde
    Serial.print("Nächste Synchronisation mit RTC: ");
    PrintDateTime(TimeSyncMitRTC);
    Serial.println();
  }
}



